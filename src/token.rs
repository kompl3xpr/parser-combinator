#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Token {
    Ident(String),
    LitExpr(i32),
    True,
    False,
    Sign(Sign),
    Keyword(Keyword),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Sign {
    Lt,
    Gt,
    LtOrEq,
    GtOrEq,
    BraceL,
    BraceR,
    ParenL,
    ParenR,
    Equal,
    Plus,
    Minus,
    Asterisk,
    Slash,
    Semicolon,
    Assign,
    ArrowR,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Keyword {
    Var,
    Until,
    Case,
    Put,
    Otherwise,
}

#[macro_export]
macro_rules! tokens {
    ($($tokens: tt)*) => {{
        use $crate::token::{Token::*, Sign::*, Keyword::*};
        let mut ret: Vec<$crate::token::Token> = vec![];
        push_tokens!(ret; $($tokens)* ,);
        ret
    }};
}

#[macro_export]
macro_rules! push_tokens {
    ($v: expr;) => {};
    ($v: expr; ,) => {};
    ($v: expr; ,,) => {};

    ($v: expr; #($e: expr) $($others: tt)*) => {
        $v.push($crate::token::Token::LitExpr($e));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; #T $($others: tt)*) => {
        $v.push($crate::token::Token::True);
        push_tokens!($v; $($others)*);
    };

    ($v: expr; #F $($others: tt)*) => {
        $v.push($crate::token::Token::False);
        push_tokens!($v; $($others)*);
    };

    ($v: expr; @ $id: ident $($others: tt)*) => {
        $v.push($crate::token::Token::Ident(stringify!($id).to_string()));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; var $($others: tt)*) => {
        $v.push(Keyword(Var));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; until $($others: tt)*) => {
        $v.push(Keyword(Until));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; case $($others: tt)*) => {
        $v.push(Keyword(Case));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; otherwise $($others: tt)*) => {
        $v.push(Keyword(Otherwise));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; put $($others: tt)*) => {
        $v.push(Keyword(Put));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; := $($others: tt)*) => {
        $v.push(Sign(Assign));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; -> $($others: tt)*) => {
        $v.push(Sign(ArrowR));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; = $($others: tt)*) => {
        $v.push(Sign(Equal));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; + $($others: tt)*) => {
        $v.push(Sign(Plus));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; - $($others: tt)*) => {
        $v.push(Sign(Minus));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; * $($others: tt)*) => {
        $v.push(Sign(Asterisk));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; / $($others: tt)*) => {
        $v.push(Sign(Slash));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; < $($others: tt)*) => {
        $v.push(Sign(Lt));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; > $($others: tt)*) => {
        $v.push(Sign(Gt));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; >= $($others: tt)*) => {
        $v.push(Sign(GtOrEq));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; <= $($others: tt)*) => {
        $v.push(Sign(LtOrEq));
        push_tokens!($v; $($others)*);
    };

    ($v: expr; ( $($others: tt)* ) $($others2: tt)*) => {
        $v.push(Sign(ParenL));
        push_tokens!($v; $($others)*);
        $v.push(Sign(ParenR));
        push_tokens!($v; $($others2)*);
    };

    ($v: expr; { $($others: tt)* } $($others2: tt)*) => {
        $v.push(Sign(BraceL));
        push_tokens!($v; $($others)*);
        $v.push(Sign(BraceR));
        push_tokens!($v; $($others2)*);
    };

    ($v: expr; ; $($others: tt)*) => {
        $v.push(Sign(Semicolon));
        push_tokens!($v; $($others)*);
    };
}
