use crate::{token::Token, parser};

#[derive(Debug)]
pub struct SimpleTokenCursor {
    inner: Vec<Token>,
    index: usize,
}

impl SimpleTokenCursor {
    pub fn new<I: IntoIterator<Item = Token>>(tokens: I) -> Self {
        Self {
            inner: Vec::from_iter(tokens.into_iter()),
            index: 0,
        }
    }
}

impl parser::TokenCursor<Token> for SimpleTokenCursor {
    type Index = usize;

    fn next(&mut self) -> Option<Token> {
        self.inner.get(self.index).map(|token| {
            self.index += 1;
            token.clone()
        })
    }

    fn has_next(&self) -> bool {
        self.inner.get(self.index).is_some()
    }

    fn move_to(&mut self, index: usize) {
        self.index = index;
    }

    fn current_index(&self) -> usize {
        self.index
    }

    fn current(&self) -> Option<Token> {
        self.inner.get(self.index).map(Clone::clone)
    }
}
