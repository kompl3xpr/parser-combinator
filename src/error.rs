use crate::{parser::TokenCursor, token::Token};
use std::fmt::{Debug, Display};

#[derive(Debug, Clone)]
pub enum ParseError<Idx> {
    UnexpectedToken {
        index: Idx,
        token: Token,
        while_parsing: String,
    },
    FailedToParseAll,
    ReachEndWhenParsing(String),
}

impl<Idx: Debug> std::error::Error for ParseError<Idx> {}

impl<Idx: Debug> Display for ParseError<Idx> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::UnexpectedToken {
                index,
                token,
                while_parsing,
            } => {
                write!(
                    f,
                    "Unexpected token `{:?}`(index: {:?}) while parsing {}.",
                    token, index, while_parsing
                )
            }
            Self::FailedToParseAll => {
                write!(f, "Failed to parse all tokens.")
            }
            Self::ReachEndWhenParsing(parsing) => {
                write!(f, "Tokens reached end while parsing {}", parsing)
            }
        }
    }
}

impl<Idx> ParseError<Idx> {
    pub fn new<C, S>(tokens: &mut C, while_parsing: S) -> Self
    where
        C: TokenCursor<Token, Index = Idx>,
        S: ToOwned<Owned = String>,
    {
        match tokens.current() {
            Some(token) => Self::UnexpectedToken {
                index: tokens.current_index(),
                token,
                while_parsing: while_parsing.to_owned(),
            },
            _ => Self::ReachEndWhenParsing(while_parsing.to_owned())
        }
    }

    pub fn failed_to_parse_all() -> Self {
        Self::FailedToParseAll
    }
}

impl<I> Default for ParseError<I> {
    fn default() -> Self {
        Self::failed_to_parse_all()
    }
}
