mod ast;
mod error;
mod parser;
use parser::{Parser, stmt::Any as Statement};
mod token;
mod cursor;
use cursor::SimpleTokenCursor;

fn main() {
    let mut tokens = SimpleTokenCursor::new(tokens![{
        var @a := #(10);
        var @b := #(1) + @a * #(10);
        until (@a >= @b + #(3)) {
            @a := @a + #(1);
            put @a;
        }
        case ((@a < #(10)) = #F) {
            #T -> put @a + #(6) * @b;
            otherwise -> {
                @a := @a - #(5);
                put @a;
            }
        }
    }]);

    // Block([
    //   NewVarAssign("a", LitNum(10)),
    //   NewVarAssign("b", Add(LitNum(1), Mul(Var("a"), LitNum(10)))),
    //   Until(GtOrEq(Var("a"), Add(Var("b"), LitNum(3))), Block([
    //     Assign("a", Add(Var("a"), LitNum(1))),
    //     Put(Var("a"))
    //   ])),
    //   Case(Eq(Lt(Var("a"), LitNum(10)), False), [(
    //       True, Put(Add(Var("a"), Mul(LitNum(6), Var("b"))))
    //     )],
    //     Some(Block([
    //       Assign("a", Sub(Var("a"), LitNum(5))),
    //       Put(Var("a"))
    //     ]))
    //   )
    // ])
    println!("{:?}", Statement.parse_all(&mut tokens).unwrap());
}
