pub(self) mod basic;
pub(self) mod binop;
pub(self) mod cmp;
pub mod expr;
pub mod stmt;

pub trait TokenCursor<Token> {
    type Index;
    fn has_next(&self) -> bool;
    fn next(&mut self) -> Option<Token>;
    fn move_to(&mut self, index: Self::Index);
    fn current_index(&self) -> Self::Index;
    fn current(&self) -> Option<Token>;
}

pub trait Parser<Token>: Sized {
    type Out;
    type Err;

    fn parse<T: TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err>;

    fn parse_all<T>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err>
    where
        T: TokenCursor<Token>,
        Self::Err: Default,
    {
        let index = tokens.current_index();
        let ret = self.parse(tokens)?;
        match tokens.has_next() {
            true => {
                tokens.move_to(index);
                Err(Default::default())
            }
            _ => Ok(ret),
        }
    }

    fn or<Other>(self, other: Other) -> Or<Self, Other>
    where
        Other: Parser<Token, Out = Self::Out, Err = Self::Err>,
    {
        Or(self, other)
    }

    fn and_then<Other, F, Out>(self, other: Other, f: F) -> AndThen<Self, Other, F>
    where
        Other: Parser<Token, Err = Self::Err>,
        F: Fn(Self::Out, <Other as Parser<Token>>::Out) -> Out,
    {
        AndThen(self, other, f)
    }

    fn map<F, Out>(self, f: F) -> Map<Self, F>
    where
        F: Fn(Self::Out) -> Out,
    {
        Map(self, f)
    }
}

pub struct Or<P, Q>(P, Q);

impl<T, P, Q, O, E> Parser<T> for Or<P, Q>
where
    P: Parser<T, Out = O, Err = E>,
    Q: Parser<T, Out = O, Err = E>,
{
    type Out = O;
    type Err = E;

    fn parse<U: TokenCursor<T>>(&self, tokens: &mut U) -> Result<O, E> {
        match self.0.parse(tokens) {
            Ok(ret) => Ok(ret),
            Err(_) => self.1.parse(tokens),
        }
    }
}

pub struct AndThen<P, Q, F>(P, Q, F);

impl<T, P, Q, F, O1, O2, O, E> Parser<T> for AndThen<P, Q, F>
where
    P: Parser<T, Out = O1, Err = E>,
    Q: Parser<T, Out = O2, Err = E>,
    F: Fn(O1, O2) -> O,
{
    type Out = O;
    type Err = E;

    fn parse<U: TokenCursor<T>>(&self, tokens: &mut U) -> Result<O, E> {
        let index = tokens.current_index();
        let p = self.0.parse(tokens)?;
        match self.1.parse(tokens) {
            Ok(q) => Ok(self.2(p, q)),
            Err(e) => {
                tokens.move_to(index);
                Err(e)
            }
        }
    }
}

pub struct Map<P, F>(P, F);

impl<T, P, F, O1, O2, E> Parser<T> for Map<P, F>
where
    P: Parser<T, Out = O1, Err = E>,
    F: Fn(O1) -> O2,
{
    type Out = O2;
    type Err = E;

    fn parse<U: TokenCursor<T>>(&self, tokens: &mut U) -> Result<O2, E> {
        (self.0.parse(tokens)).map(&self.1)
    }
}
