#[derive(Debug)]
pub enum Expr {
    LitNum(i32),
    Var(String),
    True,
    False,

    Lt(Box<Expr>, Box<Expr>),
    Gt(Box<Expr>, Box<Expr>),
    Eq(Box<Expr>, Box<Expr>),
    LtOrEq(Box<Expr>, Box<Expr>),
    GtOrEq(Box<Expr>, Box<Expr>),

    Add(Box<Expr>, Box<Expr>),
    Mul(Box<Expr>, Box<Expr>),
    Sub(Box<Expr>, Box<Expr>),
    Div(Box<Expr>, Box<Expr>),
}

#[derive(Debug)]
pub enum Stmt {
    Put(Box<Expr>),
    Block(Vec<Stmt>),
    NewVar(String),
    NewVarAssign(String, Box<Expr>),
    Assign(String, Box<Expr>),
    Case(Box<Expr>, Vec<(Box<Expr>, Box<Stmt>)>, Option<Box<Stmt>>),
    Until(Box<Expr>, Box<Stmt>),
}
