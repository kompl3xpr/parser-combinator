use crate::token::{Keyword, Sign, Token};

use super::Parser;

pub struct EmptyParser;
pub struct IdentParser;
pub struct LitNumParser;
pub struct KeywordParser(pub Keyword);
pub struct SignParser(pub Sign);
pub struct BoolParser(pub bool);

impl Parser<Token> for EmptyParser {
    type Out = ();
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, _: &mut T) -> Result<Self::Out, Self::Err> {
        Ok(())
    }
}

impl Parser<Token> for IdentParser {
    type Out = String;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        let index = tokens.current_index();
        match tokens.next() {
            Some(Token::Ident(s)) => Ok(s),
            _ => {
                tokens.move_to(index);
                Err(())
            }
        }
    }
}

impl Parser<Token> for LitNumParser {
    type Out = i32;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        let index = tokens.current_index();
        match tokens.next() {
            Some(Token::LitExpr(n)) => Ok(n),
            _ => {
                tokens.move_to(index);
                Err(())
            }
        }
    }
}

impl Parser<Token> for KeywordParser {
    type Out = ();
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        let index = tokens.current_index();
        match tokens.next() {
            Some(Token::Keyword(k)) if k == self.0 => Ok(()),
            _ => {
                tokens.move_to(index);
                Err(())
            }
        }
    }
}

impl Parser<Token> for SignParser {
    type Out = ();
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        let index = tokens.current_index();
        match tokens.next() {
            Some(Token::Sign(s)) if s == self.0 => Ok(()),
            _ => {
                tokens.move_to(index);
                Err(())
            }
        }
    }
}

impl Parser<Token> for BoolParser {
    type Out = ();
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        let index = tokens.current_index();
        match tokens.next() {
            Some(Token::True) if self.0 => Ok(()),
            Some(Token::False) if !self.0 => Ok(()),
            _ => {
                tokens.move_to(index);
                Err(())
            }
        }
    }
}
