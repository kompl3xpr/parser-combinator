use super::Parser;
use super::{basic::*, expr::*};
use crate::{ast::Expr, token::*};

struct Gt;
struct Lt;
struct Eq;
struct GtOrEq;
struct LtOrEq;
struct Operand;

pub struct Cmp;
impl Parser<Token> for Cmp {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Gt.or(Lt).or(Eq).or(GtOrEq).or(LtOrEq).parse(tokens)
    }
}

impl Parser<Token> for Operand {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        super::binop::AddSubItem
            .or(super::binop::MulDivItem)
            .or(Single)
            .parse(tokens)
    }
}

impl Parser<Token> for Gt {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Operand
            .and_then(SignParser(Sign::Gt), |x, _| Box::new(x))
            .and_then(Operand, |x, y| Expr::Gt(x, Box::new(y)))
            .parse(tokens)
    }
}

impl Parser<Token> for Lt {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Operand
            .and_then(SignParser(Sign::Lt), |x, _| Box::new(x))
            .and_then(Operand, |x, y| Expr::Lt(x, Box::new(y)))
            .parse(tokens)
    }
}

impl Parser<Token> for Eq {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Operand
            .and_then(SignParser(Sign::Equal), |x, _| Box::new(x))
            .and_then(Operand, |x, y| Expr::Eq(x, Box::new(y)))
            .parse(tokens)
    }
}

impl Parser<Token> for GtOrEq {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Operand
            .and_then(SignParser(Sign::GtOrEq), |x, _| Box::new(x))
            .and_then(Operand, |x, y| Expr::GtOrEq(x, Box::new(y)))
            .parse(tokens)
    }
}

impl Parser<Token> for LtOrEq {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Operand
            .and_then(SignParser(Sign::LtOrEq), |x, _| Box::new(x))
            .and_then(Operand, |x, y| Expr::LtOrEq(x, Box::new(y)))
            .parse(tokens)
    }
}
