use super::Parser;
use super::{basic::*, expr::*};
use crate::{ast::Expr, token::*};

pub struct MulDivItem;
struct MulDivItemTmp;
pub struct AddSubItem;
struct AddSubItemTmp;

impl Parser<Token> for MulDivItem {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Single
            .and_then(MulDivItemTmp, |expr, v| {
                v.into_iter().rev().fold(expr, |expr, (op, ex)| match op {
                    Sign::Asterisk => Expr::Mul(Box::new(expr), Box::new(ex)),
                    Sign::Slash => Expr::Div(Box::new(expr), Box::new(ex)),
                    _ => unreachable!(),
                })
            })
            .parse(tokens)
    }
}

impl Parser<Token> for MulDivItemTmp {
    type Out = Vec<(Sign, Expr)>;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        let mul = SignParser(Sign::Asterisk)
            .and_then(Single, |_, x| x)
            .and_then(MulDivItemTmp, |x, mut vec| {
                vec.push((Sign::Asterisk, x));
                vec
            });
        let div = SignParser(Sign::Slash).and_then(Single, |_, x| x).and_then(
            MulDivItemTmp,
            |x, mut vec| {
                vec.push((Sign::Slash, x));
                vec
            },
        );
        let empty = EmptyParser.map(|_| vec![]);
        mul.or(div).or(empty).parse(tokens)
    }
}

impl Parser<Token> for AddSubItem {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        MulDivItem
            .or(Single)
            .and_then(AddSubItemTmp, |expr, v| {
                v.into_iter().rev().fold(expr, |expr, (op, ex)| match op {
                    Sign::Plus => Expr::Add(Box::new(expr), Box::new(ex)),
                    Sign::Minus => Expr::Sub(Box::new(expr), Box::new(ex)),
                    _ => unreachable!(),
                })
            })
            .parse(tokens)
    }
}

impl Parser<Token> for AddSubItemTmp {
    type Out = Vec<(Sign, Expr)>;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        let add = SignParser(Sign::Plus)
            .and_then(MulDivItem.or(Single), |_, x| x)
            .and_then(AddSubItemTmp, |x, mut vec| {
                vec.push((Sign::Plus, x));
                vec
            });
        let sub = SignParser(Sign::Minus)
            .and_then(MulDivItem.or(Single), |_, x| x)
            .and_then(AddSubItemTmp, |x, mut vec| {
                vec.push((Sign::Minus, x));
                vec
            });
        let empty = EmptyParser.map(|_| vec![]);
        add.or(sub).or(empty).parse(tokens)
    }
}
