use super::{
    basic::{EmptyParser, IdentParser, KeywordParser, SignParser},
    expr::{Any as AnyExpr, Wrapped},
    Parser,
};
use crate::{
    ast::{self, Stmt},
    token::*,
};

pub struct Any;
struct Stmts;
struct StmtsTmp;
struct Block;

struct Put;
struct NewVar;
struct NewVarAssign;
struct Assign;
struct Until;

struct Case;
struct Otherwise;
struct ArmsTmp;
struct Arms;

impl Parser<Token> for Any {
    type Out = Stmt;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Block
            .or(Case)
            .or(Put)
            .or(NewVar)
            .or(NewVarAssign)
            .or(Assign)
            .or(Until)
            .parse(tokens)
    }
}

impl Parser<Token> for Stmts {
    type Out = Vec<Stmt>;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Any.and_then(StmtsTmp, |x, mut vec| {
            vec.push(x);
            vec
        })
        .parse(tokens)
    }
}

impl Parser<Token> for StmtsTmp {
    type Out = Vec<Stmt>;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Any.and_then(StmtsTmp, |x, mut vec| {
            vec.push(x);
            vec
        })
        .or(EmptyParser.map(|_| vec![]))
        .parse(tokens)
    }
}

impl Parser<Token> for Block {
    type Out = Stmt;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        SignParser(Sign::BraceL)
            .and_then(Stmts, |_, x| x)
            .and_then(SignParser(Sign::BraceR), |x, _| {
                Stmt::Block(x.into_iter().rev().collect())
            })
            .parse(tokens)
    }
}

impl Parser<Token> for Put {
    type Out = Stmt;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        KeywordParser(Keyword::Put)
            .and_then(AnyExpr, |_, x| Stmt::Put(Box::new(x)))
            .and_then(SignParser(Sign::Semicolon), |s, _| s)
            .parse(tokens)
    }
}

impl Parser<Token> for NewVar {
    type Out = Stmt;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        KeywordParser(Keyword::Var)
            .and_then(IdentParser, |_, x| Stmt::NewVar(x))
            .and_then(SignParser(Sign::Semicolon), |s, _| s)
            .parse(tokens)
    }
}

impl Parser<Token> for NewVarAssign {
    type Out = Stmt;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        KeywordParser(Keyword::Var)
            .and_then(IdentParser, |_, x| x)
            .and_then(SignParser(Sign::Assign), |x, _| x)
            .and_then(AnyExpr, |s, e| Stmt::NewVarAssign(s, Box::new(e)))
            .and_then(SignParser(Sign::Semicolon), |s, _| s)
            .parse(tokens)
    }
}

impl Parser<Token> for Assign {
    type Out = Stmt;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        IdentParser
            .and_then(SignParser(Sign::Assign), |x, _| x)
            .and_then(AnyExpr, |s, e| Stmt::Assign(s, Box::new(e)))
            .and_then(SignParser(Sign::Semicolon), |s, _| s)
            .parse(tokens)
    }
}

impl Parser<Token> for Until {
    type Out = Stmt;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        KeywordParser(Keyword::Until)
            .and_then(Wrapped, |_, x| x)
            .and_then(Any, |e, s| Stmt::Until(Box::new(e), Box::new(s)))
            .parse(tokens)
    }
}

impl Parser<Token> for Case {
    type Out = Stmt;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        KeywordParser(Keyword::Case)
            .and_then(Wrapped, |_, x| Box::new(x))
            .and_then(SignParser(Sign::BraceL), |x, _| x)
            .and_then(Arms, |x, y| (x, y.into_iter().rev().collect()))
            .and_then(Otherwise, |(x, y), z| Stmt::Case(x, y, z))
            .and_then(SignParser(Sign::BraceR), |x, _| x)
            .parse(tokens)
    }
}

impl Parser<Token> for Arms {
    type Out = Vec<(Box<ast::Expr>, Box<Stmt>)>;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        AnyExpr
            .and_then(SignParser(Sign::ArrowR), |x, _| Box::new(x))
            .and_then(Any, |x, y| (x, Box::new(y)))
            .and_then(ArmsTmp, |x, mut vec| {
                vec.push(x);
                vec
            })
            .parse(tokens)
    }
}

impl Parser<Token> for ArmsTmp {
    type Out = Vec<(Box<ast::Expr>, Box<Stmt>)>;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        let tmp = AnyExpr
            .and_then(SignParser(Sign::ArrowR), |x, _| Box::new(x))
            .and_then(Any, |x, y| (x, Box::new(y)))
            .and_then(ArmsTmp, |x, mut vec| {
                vec.push(x);
                vec
            });
        tmp.or(EmptyParser.map(|_| vec![])).parse(tokens)
    }
}

impl Parser<Token> for Otherwise {
    type Out = Option<Box<Stmt>>;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        KeywordParser(Keyword::Otherwise)
            .and_then(SignParser(Sign::ArrowR), |_, _| ())
            .and_then(Any, |_, y| Some(Box::new(y)))
            .or(EmptyParser.map(|_| None))
            .parse(tokens)
    }
}
