use super::basic::*;
use super::Parser;
use crate::{ast::Expr, token::*};

pub struct Atom;
pub struct Wrapped;
pub struct Any;
pub struct Single;

impl Parser<Token> for Atom {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        LitNumParser
            .map(|n| Expr::LitNum(n))
            .or(IdentParser.map(|s| Expr::Var(s)))
            .or(BoolParser(true).map(|_| Expr::True))
            .or(BoolParser(false).map(|_| Expr::False))
            .parse(tokens)
    }
}

impl Parser<Token> for Wrapped {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        SignParser(Sign::ParenL)
            .and_then(Any, |_, x| x)
            .and_then(SignParser(Sign::ParenR), |x, _| x)
            .parse(tokens)
    }
}

impl Parser<Token> for Any {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        super::cmp::Cmp
            .or(super::binop::AddSubItem)
            .or(super::binop::MulDivItem)
            .or(Single)
            .parse(tokens)
    }
}

impl Parser<Token> for Single {
    type Out = Expr;
    type Err = ();

    fn parse<T: super::TokenCursor<Token>>(&self, tokens: &mut T) -> Result<Self::Out, Self::Err> {
        Wrapped.or(Atom).parse(tokens)
    }
}
